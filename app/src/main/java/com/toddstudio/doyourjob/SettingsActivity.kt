package com.toddstudio.doyourjob

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText

class SettingsActivity : AppCompatActivity() {
    private lateinit var mSettings: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSettings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        setContentView(R.layout.settings_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun saveTriggerClick(View: View) {
        val minutesToWork = findViewById<TextInputEditText>(R.id.minutes_to_work)
        val minutesToRelax = findViewById<TextInputEditText>(R.id.minutes_to_relax)


        val editor: SharedPreferences.Editor = mSettings.edit()

        var work = minutesToWork.text.toString().toLong()
        var relax = minutesToRelax.text.toString().toLong()

        if (minutesToWork.text.toString().toInt() > 60 || minutesToRelax.text.toString().toInt() > 30) {
            Toast.makeText(this@SettingsActivity,
                "Максимальные значения:\nДля работы — 60 минут\nДля отдыха — 30 минут\nУстановлены значения по умолчанию", Toast.LENGTH_SHORT).show()
            work = 20
            relax = 10
            editor.putLong("APP_PREFERENCE_WORK", work).apply()
            editor.putLong("APP_PREFERENCE_RELAX", relax).apply()
        } else {

            editor.putLong("APP_PREFERENCE_WORK", work).apply()
            editor.putLong("APP_PREFERENCE_RELAX", relax).apply()

            Toast.makeText(this@SettingsActivity, "Настройки сохранены, перезапустите приложение", Toast.LENGTH_LONG)
                .show()
        }
    }

}
