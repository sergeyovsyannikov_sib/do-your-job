package com.toddstudio.doyourjob

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import com.toddstudio.doyourjob.databinding.ActivityMainBinding

class TimerActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private var timer: CountDownTimer? = null
    var minutesToWork: Long = 20
    var minutesToRelax: Long = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var mSettings: SharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (mSettings.contains("APP_PREFERENCE_WORK")){
            minutesToWork = mSettings.getLong("APP_PREFERENCE_WORK", 0)
            minutesToRelax = mSettings.getLong("APP_PREFERENCE_RELAX", 0)
        }

        binding.minutesText.text = minutesToWork.toString() + ":"
        binding.secondsText.text = "00"

        binding.apply {
            timerTrigger.setOnClickListener {
                timerTrigger.text = "закончить работу"
                startWorkingTimer(minutesToWork * 60000)
            }
        }
    }

    fun settingsTriggerClick(View: View){
        var intent = Intent(this@TimerActivity, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun ifSecondsAndButtonTrigger(seconds: Long, timer: CountDownTimer?, mSettings: SharedPreferences) {
        if (seconds < 10) {
            binding.secondsText.text = "0$seconds"
        } else {
            binding.secondsText.text = "$seconds"
        }

        binding.timerTrigger.setOnClickListener {
            timer?.cancel()
            binding.stateText.text = "Остановлено"
            binding.minutesText.text = mSettings.getLong("APP_PREFERENCE_WORK", 0).toString() + ":"
            binding.secondsText.text = "00"
            binding.timerTrigger.text = "начать работу"
            binding.apply {
                timerTrigger.setOnClickListener {
                    timerTrigger.text = "закончить работу"
                    startWorkingTimer(minutesToWork * 60000)
                }
            }
        }
    }

    private fun startWorkingTimer(timeMillis: Long) {
        binding.stateText.text = "Работаем"
        timer?.cancel()
        timer = object : CountDownTimer(timeMillis, 1) {
            override fun onTick(p0: Long) {
                val seconds: Long = (p0 / 1000) % 60
                val minutes: Long = p0 / 60000
                binding.minutesText.text = "$minutes:"
                ifSecondsAndButtonTrigger(seconds, timer, mSettings = getSharedPreferences("settings", Context.MODE_PRIVATE))
            }
            override fun onFinish() {
                Toast.makeText(this@TimerActivity, "Отдыхаем!", Toast.LENGTH_SHORT).show()
                continueWorkingTimer(minutesToRelax * 60000)
            }

        }.start()
    }

    private fun continueWorkingTimer(timeMillis: Long) {
        binding.stateText.text = "Отдыхаем"
        timer = object : CountDownTimer(timeMillis, 1) {
            override fun onTick(p0: Long) {
                val seconds: Long = (p0 / 1000) % 60
                val minutes: Long = p0 / 60000
                binding.minutesText.text = "$minutes:"
                ifSecondsAndButtonTrigger(seconds, timer, mSettings = getSharedPreferences("settings", Context.MODE_PRIVATE))
            }
            override fun onFinish() {
                Toast.makeText(this@TimerActivity, "Возвращаемся к работе!", Toast.LENGTH_SHORT).show()
                startWorkingTimer(minutesToWork * 60000)
            }

        }.start()
    }
}